
Pod::Spec.new do |s|
  s.name             = 'SunLandsFlutterModule'
  s.version          = '0.1.0'
  s.summary          = 'A short description of SunLandsFlutterModule.'


  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://gitlab.com/OmgKevin/sunlandsfluttermodule.git'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'OmgKevin' => 'kunkunxing92@gmail.com' }
  s.source           = { :git => 'https://gitlab.com/OmgKevin/sunlandsfluttermodule.git', :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'
  
  s.static_framework = true
  p = Dir::open("ios_frameworks")
  arr = Array.new
  arr.push('ios_frameworks/*.framework')
  s.ios.vendored_frameworks = arr


end
