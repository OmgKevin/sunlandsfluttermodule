# SunLandsFlutterModule

[![CI Status](https://img.shields.io/travis/OmgKevin/SunLandsFlutterModule.svg?style=flat)](https://travis-ci.org/OmgKevin/SunLandsFlutterModule)
[![Version](https://img.shields.io/cocoapods/v/SunLandsFlutterModule.svg?style=flat)](https://cocoapods.org/pods/SunLandsFlutterModule)
[![License](https://img.shields.io/cocoapods/l/SunLandsFlutterModule.svg?style=flat)](https://cocoapods.org/pods/SunLandsFlutterModule)
[![Platform](https://img.shields.io/cocoapods/p/SunLandsFlutterModule.svg?style=flat)](https://cocoapods.org/pods/SunLandsFlutterModule)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

SunLandsFlutterModule is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'SunLandsFlutterModule'
```

## Author

OmgKevin, kunkunxing92@gmail.com

## License

SunLandsFlutterModule is available under the MIT license. See the LICENSE file for more info.
